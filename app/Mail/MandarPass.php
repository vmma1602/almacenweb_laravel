<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MandarPass extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     
     * @return void
     */
    public $email;
    public function __construct($emailPass)
    {
        $this->email=$emailPass;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('template.plantillacorreo');
    }
}
