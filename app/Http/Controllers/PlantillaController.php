<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Mail\MandarPass;
use App\Models\Usuarios;
use App\Models\RegistroMaterial;

class PlantillaController extends Controller
{


    public function mostrarMaterial()
    {
        $material=@RegistroMaterial::all();
        return view('contenidos.vista',['material'=>$material]);
    }
    public function mostrarUsuario()
    {
        $usuario=@Usuarios::all();
        return view('contenidos.vistausuario',['user'=>$usuario]);
    }
   
    public function index(){
        return view ('auth.sesion');
    }
    public function reset(){
        return view ('passwords.email');
    }
   
    public function enviarPassword(Request $emailPass)
    {
        $email=strval($emailPass->email);
        $pass = DB::table('users')->select('password')->whereCorreo($email)->first();   
        if($pass=='null')
        {
            Mail::to($email)->send(new MandarPass($pass));
            return redirect('/');
        }
        else{
            
            return redirect('/auth/sesion');
        }
        
    }



    public function GuardarUser(Request $DatosUsuario){
        
        $datosUser = new Usuarios();
        $datosUser->codigo = $DatosUsuario->codigo;
        $datosUser->nombre = $DatosUsuario->nombre;
        $datosUser->app = $DatosUsuario->app;
        $datosUser->apm = $DatosUsuario->apm;
        $datosUser->correo = $DatosUsuario->correo;
        $datosUser->tipo = $DatosUsuario->tipo;  
        $datosUser->password = 'Default123';          
        if($datosUser->save())
        {
            return view('contenidos.registrousuario');
        }
       
    }
    public function GuardarMaterial(Request $datosMaterial){
        
        $datosMat = new RegistroMaterial();
        $datosMat->codigoMat = $datosMaterial->codigoMat;
        $datosMat->nombreMat = $datosMaterial->nombreMat;
        $datosMat->proveedorMat = $datosMaterial->proveedorMat;
        $datosMat->tipoMat = $datosMaterial->tipoMat;
        $datosMat->ubicacionMat = $datosMaterial->ubicacionMat;
        $datosMat->cantidadMat = $datosMaterial->cantidadMat;      
        if($datosMat->save())
        {
            return view('contenidos.registro');
        }
       
    }

    public function login(Request $request)
    {
            
        $codigo = strval($request->codigo);
        $contra = strval($request->password);

        $codigouser = DB::table('users')->select('codigo','password')->whereCodigo($codigo)->first();
        if($codigouser->codigo== $codigo && $codigouser->password == $contra)
        {
           return view('contenidos.registro');
        }
        else{
            return redirect('/auth/sesion');
        }
    }
     
    public function editarMaterial($codigoMat)
    {
       
        $datosMat = DB::table('registro_material')->where('codigoMat', '=', ''.$codigoMat)->first();
        return view('contenidos.editarMaterial',['mat'=>$datosMat]);
    }

    public function updateMaterial(Request $codigoMatUpdate, $codigoMat)
    {
       
      //  $datosMat = DB::table('registro_material')->where('codigoMat', '=', ''.$codigoMat)->first();
     
        $datosMat =@RegistroMaterial::find($codigoMat);
       // DB::table('subject_user')->where('user_id', $value)->update(['auth_teacher' => 1]);
        $datosMat->nombreMat = $codigoMatUpdate->nombreMat;
        $datosMat->proveedorMat = $codigoMatUpdate->proveedorMat;
        $datosMat->tipoMat = $codigoMatUpdate->tipoMat;
        $datosMat->ubicacionMat = $codigoMatUpdate->ubicacionMat;
        $datosMat->cantidadMat = $codigoMatUpdate->cantidadMat;      
        if($datosMat->save())
        {
            return redirect('/contenidos/vista');
        }
       
    }




}
