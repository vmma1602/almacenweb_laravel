<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistroMaterialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_material', function (Blueprint $table) {
            $table->unsignedInteger('codigoMat')->primary();
            $table->string('nombreMat');
            $table->string('proveedorMat');
            $table->string('tipoMat');
            $table->unsignedDouble('cantidadMat');
            $table->string('ubicacionMat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_material');
    }
}
