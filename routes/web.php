<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlantillaController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();       // ----------------->  Ruta donde inicia
Route::get('/', function () {
    return view('contenidos.registrousuario');
});
//Rutas del login

Route::post('/passwords/email', [PlantillaController::class, 'enviarPassword'])->name('enviarPassword');
Route::get('/passwords/email', [PlantillaController::class, 'reset'])->name('reset');
Route::post('/auth/sesion', [PlantillaController::class, 'login'])->name('log');
Route::get('/auth/sesion', [PlantillaController::class, 'index'])->name('salir');

//Rutas de guardado de datos
Route::post('/contenidos/registrousuario', [PlantillaController::class, 'GuardarUser'])->name('GuardarUser');
Route::post('/contenidos/registro', [PlantillaController::class, 'GuardarMaterial'])->name('GuardarMaterial');
Route::post('/contenidos/editarMaterial/{codigo}', [PlantillaController::class, 'updateMaterial']);

//Rutas del sidenav
Route::get('/contenidos/vista', [PlantillaController::class, 'mostrarMaterial'])->name('mostrarMaterial');
Route::get('/contenidos/vistaUsuario', [PlantillaController::class, 'mostrarUsuario'])->name('mostrarUsuario');



//Editar
Route::get('/contenidos/editarMaterial/{codigo}', [PlantillaController::class, 'editarMaterial'])->name('editarMaterial');



Route::get('/contenidos/registro', function () {
    return view('contenidos.registro');})->name('registrarMaterial');
Route::get('/contenidos/registrousuario', function () {
    return view('contenidos.registrousuario');})->name('registrarUsuario');


