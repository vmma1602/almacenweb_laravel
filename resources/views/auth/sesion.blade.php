@extends('template.inicio')
@section('formLogin')

<div class="container"> 
  <div class="row">
<div class="col l2"></div>
<div class="col l8">
 
  <div class="card">
     <div class="card-header">{{ __('Inicio de sesión') }}</div>

     <div class="card-body">
         <form method="POST" action="{{ route('log') }}">
             <center>
             @csrf
             <div class="form-group"> 

                     Empleado
                     <input type="codigo" class="form-control" name="codigo" value="{{ old('codigo') }}" required autocomplete="codigo" autofocus>
             </div>

             <div class="form-group">
                 <label for="password" class="text-md-right">Contraseña</label>

                 <div>
                     <input id="password" type="password" class="form-control" name="password" required autocomplete="current-password">

                     
                 </div>
             </div>

             <div class="form-group row">
                 <div class=" offset-md-4">
                     <div class="form-check">
                         <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                         <label class="form-check-label" for="remember">
                             {{ __('Recordar usuario') }}
                         </label>
                     </div>
                 </div>
             </div>

             <div class="form-group ">
                 <div class="">
                     <button type="submit" class="btn purple-gradient">
                        Ingresar
                     </button>

                    
                         <a class="btn btn-link" style="color: red" href="{{ route('password.request') }}">
                             ¿Olvidaste tu contraseña?
                         </a>
                    
                 </div>
             </div>
         </center>
         </form>
     </div>
 </div>
  </div>
 <div class="col l2"></div>
</div>
</div>

  @endsection

