@extends('template.sidenav')
@section('contenidoGeneral')

<div class="content">
    <div class="row">
      <div class="col-md-3"></div>
    <div class="col-md-8">
      <form class="text-center border border-light p-5" action="{{route('GuardarUser')}}" method="post">
    @csrf
        <p class="h4 mb-4">Registro de Usuarios</p>
    
        <div class="row">
          <div class="col-md-6">
              <input type="text" class="form-control mb-4" placeholder="Código" name="codigo">
          </div>
          <div class="col-md-6">
              <input type="text" class="form-control mb-4" placeholder="Nombre(s)" name="nombre">
          </div>
    
          <div class="col-md-6">
            <input type="text" class="form-control mb-4" placeholder="Apellido Paterno" name="app">
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control mb-4" placeholder="Apellido Materno" name="apm">
          </div>
          <div class="col-md-6">
            <input type="text" class="form-control mb-4" placeholder="Correo" name="correo" required>
          </div>
    
    
      <div class="col-md-12">
        <label>Tipo</label>
        <select class="browser-default custom-select mb-4" name="tipo">
            
            <option value="Materialista">Materialista</option>
            <option value="Administrador">Administrador</option>
        </select>
      </div>
    
      <div class="col-md-6">
        <button class="btn btn-info" type="submit">Guardar</button>
      </div>
      
      <div class="col-md-6">
        <a class="btn btn-danger">Cancelar</a>
      </div>
      </div>
    
    </form>
    </div>
    </div>
    </div>
@endsection