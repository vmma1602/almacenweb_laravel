@extends('template.sidenav')
@section('contenidoGeneral')
<div class="content">
    <div class="row">
      <div class="col-md-3"></div>
    <div class="col-md-8">
      <form class="text-center border border-light p-5" action="{{route('GuardarMaterial')}}" method="POST">
    @csrf
        <p class="h4 mb-4">Registro de Material</p>
    
        <div class="row">
          <div class="col-md-6">
        <input type="text" class="form-control mb-4" placeholder="Código" name="codigoMat" required>
      </div>
      <div class="col-md-6">
        <input type="text" class="form-control mb-4" placeholder="Nombre" name="nombreMat" required>
      </div>
    
      <div class="col-md-12">
        <label>Proveedor</label>
        <select class="browser-default custom-select mb-4" name="proveedorMat">
            <option value="6" selected>Elige una opcion</option>
            <option value="1">Nestle</option>
            <option value="2"></option>
            <option value="3"></option>
            <option value="4"></option>
        </select>
      </div>
      <div class="col-md-6">
        <input type="text" class="form-control mb-4" placeholder="Tipo" name="tipoMat">
      </div>
      <div class="col-md-6">
        <input type="number" class="form-control mb-4" placeholder="Cantidad" name="cantidadMat">
      </div>
      <div class="col-md-12">
        <label>Ubicación</label>
        <select class="browser-default custom-select mb-4" name="ubicacionMat">
            <option value="6" selected>Elige una opcion</option>
            <option value="1">Estante 2 / Almacen 1</option>
            <option value="2"></option>
            <option value="3"></option>
            <option value="4"></option>
        </select>
      </div>
      <div class="col-md-4">
        <button class="btn btn-info" type="submit">Guardar</button>
      </div>
      <div class="col-md-4">
        <a class="btn btn-dark">Leer QR</a>
      </div>
      <div class="col-md-4">
        <a class="btn btn-danger">Cancelar</a>
      </div>
      </div>
    
    </form>
    </div>
    </div>
    </div>
@endsection