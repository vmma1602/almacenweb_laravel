@extends('template.sidenav')
@section('contenidoGeneral')
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-8">
    <div class="container">
        <table class="table table-bordered">
          <thead class="thead-dark">
           <tr>
             <th>CÓDIGO</th>
             <th>NOMBRE</th>
             <th>APELLIDO PATERNO</th>
             <th>APELLIDO MATERNO</th>
             <th>TIPO</th>
             <th>CONTRASEÑA</th>
             <th>ACCION</th>
           </tr>
          </thead>
          <tbody>
              @foreach ($user as $user)
                  <tr>
                      <th>{{$user->codigo}}</th>
                      <th>{{$user->nombre}}</th>
                      <th>{{$user->app}}</th>
                      <th>{{$user->apm}}</th>
                      <th>{{$user->tipo}}</th>
                      <th>*******</th>
                  </tr>
              @endforeach
          </tbody>
        </table>
        <center><a class="btn btn-danger col-md-4" href="#">Eliminar</a></center>
        <br><br>
        <div class="text-center">
        <button type="button" class="btn btn-outline-deep-orange btn-rounded waves-effect"><i class="far fa-file-pdf mr-1"></i> Exportar PDF</button>
        <button type="button" class="btn btn-outline-dark-green btn-rounded waves-effect"><i class="far fa-file-excel mr-1"></i> Exportar Excel</button>
    </div>
    </div>
    </div>
    </div>

@endsection