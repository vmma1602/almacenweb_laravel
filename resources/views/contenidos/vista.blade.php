@extends('template.sidenav')
@section('contenidoGeneral')
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-8">
    <div class="container">
        <table class="table table-bordered">
          <thead class="thead-dark">
           <tr>
             <th>CÓDIGO</th>
             <th>NOMBRE</th>
             <th>PROVEEDOR</th>
             <th>TIPO</th>
             <th>CANTIDAD</th>
             <th>UBICACIÓN</th>
             <th>ACCION</th>
           </tr>
          </thead>
          <tbody>
              @foreach ($material as $mat)
            <tr>
              <th>{{$mat->codigoMat}}</th>
              <th>{{$mat->nombreMat}}</th>
              <th>{{$mat->proveedorMat}}</th>
              <th>{{$mat->tipoMat}}</th>
              <th>{{$mat->cantidadMat}}</th>
              <th>{{$mat->ubicacionMat}}</th>
              <th>
              <a href="{{url('/contenidos/editarMaterial/'.$mat->codigoMat)}}" class="btn btn-info">Editar</a>
            </th>
             
            </tr>
              @endforeach
          </tbody>
        </table>
        <center><a class="btn btn-danger col-md-4" href="#">Eliminar</a></center>
        <br><br>
        <div class="text-center">
        <button type="button" class="btn btn-outline-deep-orange btn-rounded waves-effect"><i class="far fa-file-pdf mr-1"></i> Exportar PDF</button>
        <button type="button" class="btn btn-outline-dark-green btn-rounded waves-effect"><i class="far fa-file-excel mr-1"></i> Exportar Excel</button>
    </div>
    </div>
    </div>
    </div>
    @endsection